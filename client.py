import socket

HOST = 'localhost'  # Listen on all available interfaces
PORT = 12345 #port จำลอง

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((HOST, PORT))
client_socket.sendall(b'a.txt')
data = client_socket.recv(1024)
file_contents = b""

while data:
    file_contents += data
    data = client_socket.recv(1024)

client_socket.close()

print('received data from server: ')
print(file_contents.decode('utf-8'))
