import socket

HOST = 'localhost'  # Listen on all available interfaces
PORT = 12345 #port จำลอง


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen(5)
print("Server is listening on {}:{}".format(HOST, PORT))

while True:
    print("Waiting for connection")
    connection, client_address = server_socket.accept()
    print("Connected by:", client_address)

    try:
        print("connection from", client_address)
        file_name = connection.recv(1024).decode('utf-8')
        with open(file_name,'rb') as file:
            file_contents = file.read()
        connection.sendall(file_contents)
    finally:
        connection.close()
        print("Closed connection")

